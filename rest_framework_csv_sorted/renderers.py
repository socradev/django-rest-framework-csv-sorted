from ordereddict import OrderedDict
from rest_framework_csv.renderers import CSVRenderer


class SortedCSVRenderer(CSVRenderer):
    """
    Renderer which serializes to Sorted CSV
    """

    media_type = 'text/csv'
    format = 'csv'
    level_sep = '.'

    def tablize(self, data):
        """
        Convert a list of data into a table.
        """
        if data:

            # First, flatten the data (i.e., convert it to a list of
            # dictionaries that are each exactly one level deep).  The key for
            # each item designates the name of the column that the item will
            # fall into.
            data = self.flatten_data(data)

            # Get the set of all unique headers, and sort them.
            headers = []
            if data[0]:
                headers = data[0].keys()

            # Create a row for each dictionary, filling in columns for which the
            # item has no data with None values.
            rows = []
            for item in data:
                row = []
                for key in headers:
                    row.append(item.get(key, None))
                rows.append(row)

            # Return your "table", with the headers as the first row.
            return [headers] + rows

        else:

            return []

    def flatten_dict(self, d):
        flat_dict = OrderedDict()
        for key, item in d.iteritems():
            key = str(key)
            flat_item = self.flatten_item(item)
            nested_item = self.nest_flat_item(flat_item, key)
            flat_dict.update(nested_item)
        return flat_dict
