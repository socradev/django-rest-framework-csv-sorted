DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    },
}

INSTALLED_APPS = (
    'rest_framework_csv_sorted',
)

SECRET_KEY = "not-so-secret"
